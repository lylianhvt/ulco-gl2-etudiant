#pragma once

#include <string>
#include <vector>

class Itemable {
    public:
        virtual std::vector<std::string> getItems() const = 0;
};

