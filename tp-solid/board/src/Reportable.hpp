#pragma once

#include <string>
#include <vector>
#include "Itemable.hpp"

class Reportable{

    virtual void report(Itemable &i) = 0;

}