#include <fstream>
#include <iostream>
#pragma once

#include "Itemable.hpp"

class ReportStdout {
    public:
    void report(Itemable &i) {
        for (const std::string & item : i.getItems())
            std::cout << item << std::endl;
            std::cout << std::endl;
        }
};