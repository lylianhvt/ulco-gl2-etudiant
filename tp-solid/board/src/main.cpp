
#include "Board.hpp"
#include "ReportFile.hpp"
#include "ReportStdout.hpp"
#include "NumBoard.hpp"

void testBoard(Board & b, ReportFile &rf) {
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");
    rf.report(b);
}

void testNumBoard(NumBoard & b1, ReportFile &rf) {
    std::cout << b1.getTitle() << std::endl;
    b1.add("test 1");
    b1.add("test 2");
    rf.report(b1);
}

int main() {

    Board b1;
    ReportFile rf("tmp.txt");
    ReportFile rf2("tmpnum.txt");   
    NumBoard b2;
    testBoard(b1, rf);
    testNumBoard(b2,rf2);

    return 0;
}

