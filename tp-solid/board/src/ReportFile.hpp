#pragma once

#include "Itemable.hpp"
#include <fstream>
#include <iostream>

class ReportFile {
public:
    ReportFile(const std::string & filename) : _ofs(filename) {}

    void report(Itemable &i) {
        for (const std::string & item : i.getItems())
            _ofs << item << std::endl;
            _ofs << std::endl;
        }
private:
    std::ofstream _ofs;
};