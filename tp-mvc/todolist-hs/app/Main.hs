import Board
import View

import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    putStrLn ""
    printBoard b
    test <- getLine
    case words test of
        ("add" : xs) -> do
            let (taskId, newBoard) = addTodo (unwords xs) b
            loop newBoard
        ("done" : xs) -> do
            let taskId = read $ unwords xs in
                let (_, b2) = toDone taskId b in
                    loop b2
        _ -> do
            putStrLn "action invalide."
            loop b