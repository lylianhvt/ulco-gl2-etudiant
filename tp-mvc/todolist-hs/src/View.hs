module View where
    
import Board
import Task

showTask :: Task -> String
showTask t = show (_taskId t) ++ ". " ++ (_taskName t)

printBoard :: Board -> IO ()
printBoard b = do
    putStrLn "Todo: "
    let (Board boardId boardTodo boardDone) = b
    mapM_ (putStrLn . showTask) boardTodo
    putStrLn "Done: "
    mapM_ (putStrLn . showTask) boardDone
