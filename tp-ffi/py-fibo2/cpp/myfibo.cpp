#include <cassert>

int fiboNaive(int n) {
    assert(n => 0);
    return n < 2 ? n : fiboNaive(n-1) + fiboNaive(n-2);
}

int fibo_iterative(int n) {
    int a = 0, b = 1;
    for (int i = 0; i < n; i++) {
        int temp = a;
        a = b;
        b = temp + b;
    }
    return a;
}


#include <pybind11/pybind11.h>

PYBIND11_MODULE(myfibo, m) {

    // TODO export fiboNaive (as fibo_naive)
    m.doc() = "fibo";
    m.def("fiboNaive", &fiboNaive, "fibo");

    // TODO export fiboIterative (as fibo_iterative)

    m.doc() = "fibo it";
    m.def("fibo_iterative", &fibo_iterative, "fibo it");

}

