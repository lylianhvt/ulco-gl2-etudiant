#include "Logger.hpp"

#include <algorithm>
#include <iomanip>
#include <map>
#include <sstream>

void Logger::addItem(Level l, const std::string & m) {
  // TODO implement Logger::addItem
  _items.emplace_back(l, m);
}

std::string Logger::reportByAdded() const {
  // TODO Logger::reportByAdded
      std::string report;
    for (const auto & [level, message] : _items) {
        switch (level) {
            case Level::Info:
                report += "[INFO] ";
                break;
            case Level::Warning:
                report += "[WARNING] ";
                break;
            case Level::Error:
                report += "[ERROR] ";
                break;
        }
        report += message + "\n";
    }
    return report;
}

std::string Logger::reportByLevel() const {
  // TODO Logger::reportByLevel
  return "TODO Logger::reportByLevel";
}

